//
//  HomeViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/20/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSoup
import SafariServices
import Realm
import RealmSwift
import MaterialComponents
import Firebase

class HomeViewController: UITableViewController {
    let ACTIVITY_SECTION = 2
    let NEWS_SECTION = 1
    let MISC_SECTION = 0
    
    var blogPosts = [BlogPost]()
//    var activityList = [Activities]()
    var blogPostLoading = false
//    var activityLoading = false
    
    var activityIndicator: UIActivityIndicatorView?
    
    func loadBlogPost() {
        self.blogPostLoading = true
        let url = "http://popconasia.com/wp-json/wp/v2/posts?page=1&per_page=10"

        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            self.blogPostLoading = false
            
            guard response.result.isSuccess else {
                print(response.result.error!)
                return
            }
            
            self.blogPosts.removeAll()
            
            for object in response.result.value as! [Any] {
                if let blogPost = BlogPost(JSON: object as! [String: Any]) {
                    self.blogPosts.append(blogPost)
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
//    func loadActivity() {
//        self.activityLoading = true
//        let url = "http://popconasia.com/wp-json/wp/v2/activities/?page=1&per_page=3"
//        
//        Alamofire.request(url).responseJSON(completionHandler: {
//            response in
//            
//            self.activityLoading = false
//            
//            guard response.result.isSuccess else {
//                print(response.result.error!)
//                return
//            }
//            
//            self.activityList.removeAll()
//            for object in response.result.value as! [Any] {
//                if let activity = Activities.Parse(from: object as! [String: Any]) {
//                    self.activityList.append(activity)
//                }
//            }
//            
//            self.tableView.reloadData()
//        })
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.tableView.backgroundView = self.activityIndicator
        self.activityIndicator?.startAnimating()
        loadBlogPost()
//        loadActivity()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.blogPostLoading ? 0 : 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
//        case ACTIVITY_SECTION:
//            return activityList.count
        case NEWS_SECTION:
            return blogPosts.count
        case MISC_SECTION:
            return 0
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
//        case ACTIVITY_SECTION:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityCell", for: indexPath)
//            let item = self.activityList[indexPath.row]
//            let featuredImage = cell.viewWithTag(1) as! UIImageView
//            featuredImage.image = #imageLiteral(resourceName: "popo")
//            if let url = URL(string: item.featuredMediaUrl!) {
//                featuredImage.af_setImage(withURL: url)
//            }
//            let title = cell.viewWithTag(2) as! UILabel
//            if let titleText = try? SwiftSoup.parse(item.title!) {
//                title.text = try? titleText.text()
//            }
//            return cell
        case NEWS_SECTION:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath)
            let item = self.blogPosts[indexPath.row]
            let featuredImage = cell.viewWithTag(1) as! UIImageView
            featuredImage.image = #imageLiteral(resourceName: "popo")
            if let url = URL(string: item.featuredMediaUrl!) {
                featuredImage.af_setImage(withURL: url)
            }
            let title = cell.viewWithTag(2) as! UILabel
            if let titleText = try? SwiftSoup.parse(item.title!) {
                title.text = try? titleText.text()
            }
            return cell
        case MISC_SECTION:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MiscCell", for: indexPath)
            let button = cell.viewWithTag(1) as! UIButton
            button.addTarget(self, action: #selector(HomeViewController.buyTicket(button:)), for: .touchUpInside)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
//        case ACTIVITY_SECTION:
//            return UITableViewAutomaticDimension
        case NEWS_SECTION:
            return UITableViewAutomaticDimension
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case MISC_SECTION:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib(nibName: "newsHeaderSection", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        let label = view.viewWithTag(1) as! UILabel
        switch section {
//        case ACTIVITY_SECTION:
//            label.text = "ACTIVITIES"
        case NEWS_SECTION:
            label.text = "LATEST NEWS"
        default:
            let bigView = UINib(nibName: "bigPopconHeader", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
            return bigView
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
//        case ACTIVITY_SECTION:
//            return "Activities"
        case NEWS_SECTION:
            return "Latest News"
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UINib(nibName: "newsFooterSection", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        let button = view.viewWithTag(1) as! UIButton
        switch section {
//        case ACTIVITY_SECTION:
//            button.setTitle("More Activities", for: .normal)
//            button.addTarget(self, action: #selector(HomeViewController.showEventViewContoller(button:)), for: .touchUpInside)
        case NEWS_SECTION:
            button.setTitle("More News", for: .normal)
            button.addTarget(self, action: #selector(HomeViewController.showNewsController(button:)), for: .touchUpInside)
        default:
            let blankView = UIView()
            blankView.backgroundColor = UIColor.white
            return blankView
        }
        return view
    }
    
//    func showEventViewContoller(button: UIButton)
//    {
//        if let navigationController = self.parent as? UINavigationController {
//            navigationController.performSegue(withIdentifier: "ShowEvent", sender: nil)
//        }
//    }
    
    func showNewsController(button: UIButton)
    {
        if let navigationController = self.parent as? UINavigationController {
            navigationController.performSegue(withIdentifier: "ShowNews", sender: nil)
        }
    }
    
    func buyTicket(button: UIButton)
    {
        Analytics.logEvent(AnalyticsEventPresentOffer, parameters: [
            AnalyticsParameterItemID: "1" as NSObject,
            AnalyticsParameterItemName: "Buy Popcon Ticket" as NSObject,
            AnalyticsParameterItemCategory: "Ticket" as NSObject]
        )
        let svc = SFSafariViewController(url: URL(string: "https://popcon.asia/ticket")!, entersReaderIfAvailable: true)
        svc.preferredBarTintColor = UIColor.init(red: 246/252, green: 172/252, blue: 75/252, alpha: 1.0)
        svc.preferredControlTintColor = UIColor.init(red: 40/252, green: 56/252, blue: 131/252, alpha: 1.0)
        self.present(svc, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
//        case ACTIVITY_SECTION:
//            return "More Activities"
        case NEWS_SECTION:
            return "More News"
        default:
            return nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPost" {
            let controller = segue.destination as! NewsDetailTableViewController
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                let item = blogPosts[indexPath.row]
                var title: String? = nil
                if let titleText = try? SwiftSoup.parse(item.title!) {
                    title = try? titleText.text()
                }
                if item.id != nil {
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemID: "\(item.id!)" as NSObject,
                        AnalyticsParameterItemName: "\(title ?? "-")" as NSObject,
                        AnalyticsParameterContentType: "BlogPost, Home" as NSObject
                        ])
                }
                controller.blogPost = blogPosts[indexPath.row]
            }
        }
//        else if segue.identifier == "ShowEvent" {
//            let controller = segue.destination as! ActivityDetailTableViewController
//            
//            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
//                controller.activity = activityList[indexPath.row]
//            }
//        }
    }
}
