//
//  Schedule.swift
//  popcon
//
//  Created by Brata Saputra on 5/9/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class Schedule: Object {
    dynamic var id: Int = 0
    dynamic var slug: String? = nil
    dynamic var link: String? = nil
    dynamic var title: String? = nil
    dynamic var content: String? = nil
    dynamic var timeStart1: String? = nil
    dynamic var timeEnd1: String? = nil
    dynamic var timeStart2: String? = nil
    dynamic var timeEnd2: String? = nil
    dynamic var timeStart3: String? = nil
    dynamic var timeEnd3: String? = nil
    
    convenience init(id: Int, slug: String?, link: String?, title: String?, content: String?, timeStart1: String?, timeEnd1: String?, timeStart2: String?, timeEnd2: String, timeStart3: String?, timeEnd3: String?) {
        self.init()
        self.id = id
        self.slug = slug
        self.link = link
        self.title = title
        self.content = content
        self.timeStart1 = timeStart1
        self.timeEnd1 = timeEnd1
        self.timeStart2 = timeStart2
        self.timeEnd2 = timeEnd2
        self.timeStart3 = timeStart3
        self.timeEnd3 = timeEnd3
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func Parse(from json: [String: Any]) -> Schedule? {
        let schedule = Schedule()
        guard json["id"] as? Int != nil else {
            return nil
        }
        schedule.id = json["id"] as! Int
        if let slug = json["slug"] as? String {
            schedule.slug = slug
        }
        if let link = json["link"] as? String {
            schedule.link = link
        }
        if let timeStart1 = json["time_start_1"] as? String {
            schedule.timeStart1 = timeStart1
        }
        if let timeEnd1 = json["time_end_1"] as? String {
            schedule.timeEnd1 = timeEnd1
        }
        if let timeStart2 = json["time_start_2"] as? String {
            schedule.timeStart2 = timeStart2
        }
        if let timeEnd2 = json["time_end_2"] as? String {
            schedule.timeEnd2 = timeEnd2
        }
        if let timeStart3 = json["time_start_3"] as? String {
            schedule.timeStart3 = timeStart3
        }
        if let timeEnd3 = json["time_end_3"] as? String {
            schedule.timeEnd3 = timeEnd3
        }
        if let title = json["title"] as? [String: Any] {
            if let rendered = title["rendered"] as? String {
                schedule.title = rendered
            }
        }
        if let content = json["content"] as? [String: Any] {
            if let rendered = content["rendered"] as? String {
                schedule.content = rendered
            }
        }
        return schedule
    }
//    required init?(map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        if map.JSON["id"] != nil {
//            id <- map["id"]
//        }
//        if map.JSON["slug"] != nil {
//            slug <- map["slug"]
//        }
//        if map.JSON["link"] != nil {
//            link <- map["link"]
//        }
//        if map.JSON["time_start_1"] != nil {
//            timeStart1 <- map["time_start_1"]
//        }
//        if map.JSON["time_end_1"] != nil {
//            timeEnd1 <- map["time_end_1"]
//        }
//        if map.JSON["time_start_2"] != nil {
//            timeStart2 <- map["time_start_2"]
//        }
//        if map.JSON["time_end_2"] != nil {
//            timeEnd2 <- map["time_end_2"]
//        }
//        if map.JSON["time_start_3"] != nil {
//            timeStart3 <- map["time_start_3"]
//        }
//        if map.JSON["time_end_3"] != nil {
//            timeEnd3 <- map["time_end_3"]
//        }
//        if map.JSON["title"] != nil {
//            title <- map["title.rendered"]
//        }
//        if map.JSON["content"] != nil {
//            content <- map["content.rendered"]
//        }
//    }
}
