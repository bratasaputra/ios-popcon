//
//  PromotionTableViewController.swift
//  popcon
//
//  Created by Brata Saputra on 7/27/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import Firebase

private let reuseIdentifier = "Cell"

class PromotionTableViewController: UITableViewController {
    var promotionList: [Promotion]!
    var isDownloading: Bool!
    var activityIndicator: UIActivityIndicatorView?
    
    @IBAction func refreshPromotionAction(_ sender: UIBarButtonItem) {
        promotionList.removeAll()
        tableView.reloadData()
        loadPromotion()
    }
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    func loadPromotion() {
        activityIndicator?.startAnimating()
        refreshButton.isEnabled = false
        let url = URL(string: "http://popconasia.com/wp-json/wp/v2/promotion?per_page=100")
        Alamofire.request(url!).responseJSON(completionHandler: {
            response in
            guard response.result.isSuccess else {
                print(response.result.error ?? "Nothing")
                return
            }
            
            self.refreshButton.isEnabled = true
            self.activityIndicator?.stopAnimating()
            
            if let objects = response.result.value as? [Any] {
                for object in objects {
                    if let json = object as? [String: Any] {
                        if let promotion = Promotion.Parse(from: json) {
                            self.promotionList.append(promotion)
                        }
                    }
                }
            }
            
            self.tableView.reloadData()
        });
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.backgroundView = activityIndicator
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        
        promotionList = [Promotion]()
        
        loadPromotion()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return promotionList.isEmpty ? 0 : 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotionList.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        // Configure the cell...
        let item = promotionList[indexPath.row]
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        imageView.image = nil
        imageView.af_setImage(withURL: URL(string: item.featuredMediaUrl!)!)
        
        let titleView = cell.viewWithTag(2) as! UILabel
        if let title = try? SwiftSoup.parse(item.title!) {
            titleView.text = try? title.text()
        }

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowPromo" {
            let controller = segue.destination as! PromotionDetailViewController
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                let item = promotionList[indexPath.row]
                
                if let promotionTitle = try? SwiftSoup.parse(item.title!) {
                    let itemName = try? promotionTitle.text()
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemID: "\(item.id)" as NSObject,
                        AnalyticsParameterItemName: "\(itemName ?? "\(item.id)")" as NSObject,
                        AnalyticsParameterContentType: "Promotion" as NSObject
                        ])
                }
                
                controller.promotion = item
            }
        }
    }

}
