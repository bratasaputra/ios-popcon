//
//  ExhibitorCollectionViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/21/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import Realm
import RealmSwift
import MaterialComponents
import Firebase

private let reuseIdentifier = "Cell"

class ExhibitorCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var isDownloading : Bool!
    var currentPage : Int!
    var limit : Int!
    var exhibitorList : [Exhibitor]!
    var exhibitorListTemp : [Exhibitor]!
    var realm: Realm!
    var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var refreshAllButton: UIBarButtonItem!
    
    @IBAction func refreshAll(_ sender: UIBarButtonItem) {
        if self.isDownloading {
            return
        }
        exhibitorList.removeAll()
        exhibitorListTemp.removeAll()
        currentPage = 0
        collectionView?.reloadData()
        collectionViewLayout.invalidateLayout()
        loadCategory()
    }
    
    func refreshContent() {
        if exhibitorList.isEmpty {
            activityIndicator?.startAnimating()
        }
        refreshAllButton.isEnabled = false
        currentPage = currentPage + 1;
        let url = "http://popconasia.com/wp-json/wp/v2/exhibitor?page=\(currentPage!)&per_page=\(limit!)"
        
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            guard response.result.isSuccess else {
                self.reloadData()
                print(response.result.error!)
                return
            }
            
            for obj in response.result.value as! [Any] {
                if let exhibitor = Exhibitor.Parse(from: obj as! [String: Any]) {
                    self.exhibitorListTemp.append(exhibitor)
                }
            }
            
            if (response.result.value as! [Any]).count == self.limit {
                self.refreshContent()
            } else {
                try! self.realm.write {
                    self.realm.delete(self.realm.objects(Exhibitor.self))
                    self.realm.add(self.exhibitorListTemp, update: true)
                }
                self.reloadData()
            }
        })
    }
    
    func loadCategory() {
        if exhibitorList.isEmpty {
            activityIndicator?.startAnimating()
        }
        isDownloading = true
        refreshAllButton.isEnabled = false
        let url = "http://popconasia.com/wp-json/wp/v2/exhibitor_categories?per_page=\(limit!)"
        
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            guard response.result.isSuccess else {
                self.reloadData()
                print(response.result.error!)
                return
            }
            
            try! self.realm.write {
                self.realm.delete(self.realm.objects(ExhibitorCategory.self))
            }
            
            for obj in response.result.value as! [Any] {
                if let category = ExhibitorCategory.Parse(from: obj as! [String: Any]) {
                    try! self.realm.write {
                        self.realm.add(category, update: true)
                    }
                }
            }
            self.refreshContent()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isDownloading = false
        currentPage = 0
        limit = 100
        exhibitorList = [Exhibitor]()
        exhibitorListTemp = [Exhibitor]()
        realm = try! Realm()
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.collectionView?.backgroundView = self.activityIndicator
//        reloadData()
        if self.exhibitorList.isEmpty {
            loadCategory()
        }
    }
    
    func reloadData() {
        isDownloading = false
        activityIndicator?.stopAnimating()
        refreshAllButton.isEnabled = true
        
        let list = realm.objects(Exhibitor.self).sorted(byKeyPath: "title");
        self.exhibitorList.removeAll()
        self.exhibitorList.append(contentsOf: list)
        self.collectionView?.reloadData()
        collectionViewLayout.invalidateLayout()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.exhibitorList.count == 0 ? 0 : 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.exhibitorList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
        let item = self.exhibitorList[indexPath.row]
        let featureImage = cell.viewWithTag(1) as! UIImageView
        let size = CGSize(width: featureImage.frame.width, height: featureImage.frame.width)
        let filter = AspectScaledToFitSizeFilter(size: size)
        let placeholder = #imageLiteral(resourceName: "popo").af_imageAspectScaled(toFit: size)
        featureImage.af_cancelImageRequest()
        featureImage.layer.removeAllAnimations()
        featureImage.image = nil
        if item.featuredMediaUrl != nil {
            featureImage.af_setImage(withURL: URL(string: item.featuredMediaUrl!)!, placeholderImage: placeholder, filter: filter)
        }
        let label = cell.viewWithTag(2) as! UILabel
        label.text = nil
        if let labelText = try? SwiftSoup.parse(item.title!) {
            label.text = try? labelText.text()
        }
        let booth = cell.viewWithTag(3) as! UILabel
        booth.text = nil
        booth.text = item.boothNumber ?? ""
        if item.boothNumber2 != nil {
            booth.text = booth.text?.appending("- \(item.boothNumber2!)")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = Int.init(collectionView.frame.width)
        let totalEmptySpacing = 24
        let remainingAvailableWidth = viewWidth - totalEmptySpacing
        let newCellWidth = remainingAvailableWidth / 2
        let newHeight = newCellWidth + 100
        if newCellWidth >= 145 {
            return CGSize(width: newCellWidth, height: newHeight)
        } else {
            return CGSize(width: 145, height: newHeight)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowExhibitor" {
            let controller = segue.destination as! ExhibitorDetailViewController
            
            if let indexPath = collectionView?.indexPath(for: sender as! UICollectionViewCell) {
                let exhibitor = exhibitorList[indexPath.row]
                var exhibitorTitle: String? = "-"
                if let title = try? SwiftSoup.parse(exhibitor.title!) {
                    exhibitorTitle = try? title.text()
                }
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID: "\(exhibitor.id)" as NSObject,
                    AnalyticsParameterItemName: "\(exhibitorTitle ?? "-")" as NSObject,
                    AnalyticsParameterContentType: "Exhibitor" as NSObject
                    ])
                controller.exhibitor = exhibitorList[indexPath.row]
            }
        }
    }
}
