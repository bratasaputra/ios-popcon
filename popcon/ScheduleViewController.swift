//
//  ScheduleViewController.swift
//  popcon
//
//  Created by Brata Saputra on 7/28/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let destination = segue.destination as! ScheduleTableViewController
        
        if segue.identifier == "ShowMainStage" {
            destination.location = "main-stage"
            destination.titleString = "Main Stage"
        } else if segue.identifier == "ShowParade" {
            destination.location = "parade"
            destination.titleString = "Parade"
        } else if segue.identifier == "ShowMasterclass" {
            destination.location = "masterclass"
            destination.titleString = "Masterclass"
        } else if segue.identifier == "ShowWorkshop" {
            destination.location = "workshop"
            destination.titleString = "Workshop"
        } else if segue.identifier == "ShowPopconnect" {
            destination.location = "pop-connect"
            destination.titleString = "Pop Connect"
        } else if segue.identifier == "ShowPhoto" {
            destination.location = "photo-session"
            destination.titleString = "Photo Session"
        } else if segue.identifier == "ShowSigning" {
            destination.location = "signing-session"
            destination.titleString = "Signing Session"
        }
    }
}
