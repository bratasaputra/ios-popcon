//
//  Company.swift
//  popcon
//
//  Created by Brata Saputra on 5/10/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper

class Company : Mappable {
    var id: Int?
    var link: String?
    var title: String?
    var content: String?
    var featuredMediaUrl: String?
    var slug: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        if map.JSON["id"] != nil {
            id <- map["id"]
        }
        if map.JSON["slug"] != nil {
            slug <- map["slug"]
        }
        if map.JSON["link"] != nil {
            link <- map["link"]
        }
        if map.JSON["featured_media_url"] != nil {
            featuredMediaUrl <- map["featured_media_url"]
        }
        if map.JSON["title"] != nil {
            title <- map["title.rendered"]
        } else if map.JSON["post_title"] != nil {
            title <- map["post_title"]
        }
        if map.JSON["content"] != nil {
            content <- map["content.rendered"]
        }
    }
}
