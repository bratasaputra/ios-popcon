//
//  ExhibitorCardCell.swift
//  popcon
//
//  Created by Brata Saputra on 7/25/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit

class ExhibitorCardCell: UICollectionViewCell {

    @IBOutlet weak var exhibitorImage: UIImageView!
    @IBOutlet weak var exhibitorTitle: UILabel!
    @IBOutlet weak var exhibitorBooth: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
    }

}
