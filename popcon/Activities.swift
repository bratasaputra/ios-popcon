//
//  Activities.swift
//  popcon
//
//  Created by Brata Saputra on 5/9/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class Activities: Object {
    dynamic var id: Int = 0
    dynamic var link: String? = nil
    dynamic var slug: String? = nil
    dynamic var title: String? = nil
    dynamic var content: String? = nil
    dynamic var featuredMediaUrl: String? = nil
    dynamic var tanggal: String? = nil
    dynamic var tanggalSelesai: String? = nil
    dynamic var jamMulai: String? = nil
    dynamic var jamSelesai: String? = nil
    
    convenience init(id: Int, link: String?, slug: String?, title: String?, content: String?, featuredMediaUrl: String?, tanggal: String?, tanggalSelesai: String?, jamMulai: String?, jamSelesai: String?) {
        self.init()
        self.id = id
        self.link = link
        self.title = title
        self.content = content
        self.featuredMediaUrl = featuredMediaUrl
        self.tanggal = tanggal
        self.tanggalSelesai = tanggalSelesai
        self.jamMulai = jamMulai
        self.jamSelesai = jamSelesai
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func Parse(from json: [String: Any]) -> Activities? {
        let activity = Activities()
        guard json["id"] as? Int != nil else {
            print("Cannot cast id of Activities into Int")
            return nil
        }
        activity.id = json["id"] as! Int
        if let link = json["link"] as? String {
            activity.link = link
        }
        if let slug = json["slug"] as? String {
            activity.slug = slug
        }
        if let tanggal = json["tanggal"] as? String {
            activity.tanggal = tanggal
        }
        if let tanggalSelesai = json["tanggal_selesai"] as? String {
            activity.tanggalSelesai = tanggalSelesai
        }
        if let jamMulai = json["jam_mulai"] as? String {
            activity.jamMulai = jamMulai
        }
        if let jamSelesai = json["jam_selesai"] as? String {
            activity.jamSelesai = jamSelesai
        }
        if let title = json["title"] as? [String: Any] {
            if let rendered = title["rendered"] as? String {
                activity.title = rendered
            }
        } else if let title = json["post_title"] as? String {
            activity.title = title
        }
        if let content = json["content"] as? [String: Any] {
            if let rendered = content["rendered"] as? String {
                activity.content = rendered
            }
        } else if let content = json["post_content"] as? String {
            activity.content = content
        }
        if let featuredMediaUrl = json["featured_media_url"] as? String {
            activity.featuredMediaUrl = featuredMediaUrl
        } else if let featuredMediaUrl = json["picture_no_gif"] as? String {
            activity.featuredMediaUrl = featuredMediaUrl
        }
        return activity
    }
}
