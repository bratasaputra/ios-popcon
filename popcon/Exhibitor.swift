//
//  Exhibitor.swift
//  popcon
//
//  Created by Brata Saputra on 5/10/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class ExhibitorCategory: Object {
    dynamic var id: Int = 0
    dynamic var name: String? = nil
    dynamic var shortname: String? = nil
    dynamic var desc : String? = nil
    dynamic var slug : String? = nil
    dynamic var link : String? = nil
    
    convenience init(id: Int, name: String?, shortname: String?, desc: String?, slug: String?, link: String?) {
        self.init()
        self.id = id
        self.name = name
        self.shortname = shortname
        self.desc = desc
        self.slug = slug
        self.link = link
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func Parse(from json: [String: Any]) -> ExhibitorCategory? {
        let category = ExhibitorCategory()
        guard json["id"] as? Int != nil else {
            print("Cannot cast id of ExhibitorCategory into Int")
            return nil
        }
        category.id = json["id"] as! Int
        if let name = json["name"] as? String {
            category.name = name
        }
        if let shortname = json["shortname"] as? String {
            category.shortname = shortname
        }
        if let desc = json["desc"] as? String {
            category.desc = desc
        }
        if let slug = json["slug"] as? String {
            category.slug = slug
        }
        if let link = json["link"] as? String  {
            category.link = link
        }
        return category
    }
}

class Exhibitor: Object {
    dynamic var id: Int = 0
    dynamic var link: String? = nil
    dynamic var title: String? = nil
    dynamic var content: String? = nil
    dynamic var featuredMediaUrl: String? = nil
    dynamic var boothNumber: String? = nil
    dynamic var boothNumber2: String? = nil
    dynamic var slug: String? = nil
    dynamic var category: String? = nil
    
    convenience init(id: Int, link: String?, title: String?, content: String?, featuredMediaUrl: String?, boothNumber: String?, boothNumber2: String?, slug: String?, category: String?) {
        self.init()
        self.id = id
        self.link = link
        self.title = title
        self.content = content
        self.featuredMediaUrl = featuredMediaUrl
        self.boothNumber = boothNumber
        self.boothNumber2 = boothNumber2
        self.slug = slug
        self.category = category
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func Parse(from json: [String: Any]) -> Exhibitor? {
        let exhibitor = Exhibitor()
        
        guard json["id"] as? Int != nil else {
            return nil
        }
        
        var categoryShortname: String? = nil
        exhibitor.id = json["id"] as! Int
        if let slug = json["slug"] as? String {
            exhibitor.slug = slug
        }
        if let link = json["link"] as? String {
            exhibitor.link = link
        }
        if let featuredMediaUrl = json["featured_media_url"] as? String {
            exhibitor.featuredMediaUrl = featuredMediaUrl
        }
        if let categories = json["exhibitor_categories"] as? [Int] {
            for cat in categories {
                if let exhibitorCategory = GetCategories(cat) {
                    exhibitor.category = exhibitorCategory.name
                    categoryShortname = exhibitorCategory.shortname
                }
                break
            }
        }
        if let boothNumber = json["booth_number"] as? String {
            exhibitor.boothNumber = String.init(format: "%@.%@", categoryShortname ?? "", boothNumber)
        }
        if let boothNumber2 = json["booth_number_2"] as? String {
            if boothNumber2 != "0" {
                exhibitor.boothNumber2 = String.init(format: "%@.%@", categoryShortname ?? "", boothNumber2)

            }
        }
        if let title = json["title"] as? [String: Any] {
            if let rendered = title["rendered"] as? String {
                exhibitor.title = rendered
            }
        } else if let title = json["post_title"] as? String {
            exhibitor.title = title
        }
        if let content = json["content"] as? [String: Any] {
            if let rendered = content["rendered"] as? String {
                exhibitor.content = rendered
            }
        }
        return exhibitor
    }
    
    static func GetCategories(_ cat: Int) -> ExhibitorCategory? {
        let realm = try! Realm()
        
        return realm.objects(ExhibitorCategory.self).filter("id == \(cat)").first
    }
}
