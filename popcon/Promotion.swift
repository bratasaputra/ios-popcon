//
//  Promotion.swift
//  popcon
//
//  Created by Brata Saputra on 7/26/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class Promotion: Object {
    dynamic var id: Int = 0
    dynamic var slug: String? = nil
    dynamic var link: String? = nil
    dynamic var title: String? = nil
    dynamic var content: String? = nil
    dynamic var featuredMediaUrl: String? = nil
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    convenience init(id: Int, slug: String?, link: String?, title: String?, content: String?, url: String?) {
        self.init()
        self.id = id
        self.slug = slug
        self.link = link
        self.title = title
        self.content = content
        self.featuredMediaUrl = url
    }
    
    static func Parse(from json: [String: Any]) -> Promotion? {
        let promotion = Promotion()
        guard json["id"] as? Int != nil else {
            return nil
        }
        promotion.id = json["id"] as! Int
        if let slug = json["slug"] as? String {
            promotion.slug = slug
        }
        if let link = json["link"] as? String {
            promotion.link = link
        }
        if let url = json["featured_media_url"] as? String {
            promotion.featuredMediaUrl = url
        }
        if let title = json["title"] as? [String: Any] {
            if let rendered = title["rendered"] as? String {
                promotion.title = rendered
            }
        }
        if let content = json["content"] as? [String: Any] {
            if let rendered = content["rendered"] as? String {
                promotion.content = rendered
            }
        }
        return promotion
    }
}
