//
//  NewsCardCell.swift
//  popcon
//
//  Created by Brata Saputra on 7/25/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import MaterialComponents

class NewsCardCell: UICollectionViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
        newsTitle.font = MDCTypography.titleFont()
        newsTitle.alpha = MDCTypography.titleFontOpacity()
    }

}
