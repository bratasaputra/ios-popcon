//
//  News.swift
//  popcon
//
//  Created by Brata Saputra on 5/8/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper

class BlogPost: Mappable {
    var id: Int?
    var slug: String?
    var link: String?
    var title: String?
    var content: String?
    var featuredMediaUrl: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        if map.JSON["id"] != nil {
            id <- map["id"]
        }
        if map.JSON["slug"] != nil {
            slug <- map["slug"]
        }
        if map.JSON["link"] != nil {
            link <- map["link"]
        }
        if map.JSON["title"] != nil {
            title <- map["title.rendered"]
        }
        if map.JSON["content"] != nil {
            content <- map["content.rendered"]
        }
        if map.JSON["better_featured_image"] != nil {
            featuredMediaUrl <- map["better_featured_image.source_url"]
        }
    }
}
