//
//  BlogPostDetailTableViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/9/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import SwiftSoup
import Alamofire
import AlamofireImage

enum DetailType {
    case Image
    case Paragraph
    case FigureCaption
    case Header
}

class Content : NSObject {
    var type: DetailType?
    var imageUrl: String?
    var text: String?
    var imageHeight: Int?
    var imageWidth: Int?
    
    static func createContent(from content: String) -> [Content] {
        var contents = [Content]()
        if let doc = try? SwiftSoup.parse(content) {
            let body = doc.body()
            for element in (body?.children())! {
                switch element.tagName() {
                case "p":
                    if let imageUrl = (try? element.select("img").first())! {
                        let width = try? imageUrl.attr("width")
                        let height = try? imageUrl.attr("height")
                        let url = try? imageUrl.attr("src")
                        if url != nil {
                            let imageItem = Content()
                            imageItem.type = DetailType.Image
                            imageItem.imageUrl = url
                            imageItem.imageHeight = Int.init(height!)
                            imageItem.imageWidth = Int.init(width!)
                            contents.append(imageItem)
                        }
                    } else {
                        if let content = try? element.text() {
                            let paragraphItem = Content()
                            paragraphItem.type = DetailType.Paragraph
                            paragraphItem.text = content
                            contents.append(paragraphItem)
                        }
                    }
                    break
                case "figure":
                    if let imageUrl = (try? element.select("img").first())! {
                        let width = try? imageUrl.attr("width")
                        let height = try? imageUrl.attr("height")
                        let url = try? imageUrl.attr("src")
                        let imageCaption = try? element.select("figcaption").first()?.text()
                        if url != nil && imageCaption != nil {
                            let captionItem = Content()
                            captionItem.type = DetailType.FigureCaption
                            captionItem.text = imageCaption!
                            captionItem.imageUrl = url!
                            captionItem.imageWidth = Int.init(width!)
                            captionItem.imageHeight = Int.init(height!)
                            contents.append(captionItem)
                        }
                    }
                    break
                default:
                    break
                }
            }
        }
        return contents
    }
}

class ParagraphCell : UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
    override func prepareForReuse() {
        label.text = nil
    }
}

class ImageCell : UITableViewCell {
    @IBOutlet weak var newsImage: UIImageView!
    
    override func prepareForReuse() {
        newsImage.af_cancelImageRequest()
        newsImage.layer.removeAllAnimations()
        newsImage.image = nil
    }
}

class FigureCaptionCell : UITableViewCell {
    @IBOutlet weak var figImage: UIImageView!
    @IBOutlet weak var figLabel: UILabel!
    
    override func prepareForReuse() {
        figImage.af_cancelImageRequest()
        figImage.layer.removeAllAnimations()
        figImage.image = nil
        figLabel.text = nil
    }
}

class NewsHeaderCell : UITableViewCell {
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    
    override func prepareForReuse() {
        titleText.text = nil
        titleImage.af_cancelImageRequest()
        titleImage.layer.removeAllAnimations()
        titleImage.image = nil
    }
}

class NewsDetailTableViewController: UITableViewController {
    var blogPost : BlogPost!
    var contents = [Content]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = Content()
        if let document = try? SwiftSoup.parse(blogPost.title!) {
            title.text = try? document.text()
        } else {
            title.text = blogPost.title
        }
        title.imageUrl = blogPost.featuredMediaUrl
        title.type = DetailType.Header
        contents.append(title)
        contents.append(contentsOf: Content.createContent(from: blogPost.content!))
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content = contents[indexPath.row]
        switch content.type! {
        case DetailType.Paragraph:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParagraphCell", for: indexPath) as! ParagraphCell
            cell.label.text = content.text
            return cell;
        case DetailType.Image:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
            let actualWidth = cell.newsImage.frame.size.width;
            let actualHeight = Float(actualWidth) * (Float(content.imageHeight!) / Float(content.imageWidth!))
            let size = CGSize(width: actualWidth, height: CGFloat(actualHeight))
            let filter = AspectScaledToFitSizeFilter(size: size)
            let placeholder = #imageLiteral(resourceName: "popo").af_imageAspectScaled(toFit: size)
            cell.newsImage.af_setImage(withURL: URL(string: content.imageUrl!)!, placeholderImage: placeholder, filter: filter)
            return cell
        case DetailType.FigureCaption:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FigureCell", for: indexPath) as! FigureCaptionCell
            let actualWidth = cell.figImage.frame.size.width;
            let actualHeight = Float(actualWidth) * (Float(content.imageHeight!) / Float(content.imageWidth!))
            let size = CGSize(width: actualWidth, height: CGFloat(actualHeight))
            let filter = AspectScaledToFitSizeFilter(size: size)
            let placeholder = #imageLiteral(resourceName: "popo").af_imageAspectScaled(toFit: size)
            cell.figImage.af_setImage(withURL: URL(string: content.imageUrl!)!, placeholderImage: placeholder, filter: filter)
            cell.figLabel.text = content.text
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! NewsHeaderCell
            let actualWidth = cell.titleImage.frame.size.width;
            let actualHeight = Float(actualWidth) * (Float(9) / Float(16))
            let size = CGSize(width: actualWidth, height: CGFloat(actualHeight))
            let filter = AspectScaledToFillSizeFilter(size: size)
            let placeholder = #imageLiteral(resourceName: "popo").af_imageAspectScaled(toFill: size)
            cell.titleImage.af_setImage(withURL: URL(string: content.imageUrl!)!, placeholderImage: placeholder, filter: filter)
            cell.titleText.text = content.text
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
