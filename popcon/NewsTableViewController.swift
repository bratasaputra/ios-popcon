//
//  NewsTableViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/8/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import Realm
import RealmSwift
import MaterialComponents
import Firebase

class BlogPostTableViewCell : UITableViewCell {
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    
    func configure(for post: BlogPost) {
        postTitle.text = post.title
        postImage.af_setImage(withURL: URL(string: post.featuredMediaUrl!)!)
    }
    
    override func prepareForReuse() {
        postImage.af_cancelImageRequest()
        postImage.image = nil
        postTitle.text = nil
    }
}

class NewsTableViewController: UITableViewController {
    var blogPosts = [BlogPost]()
    var isDownloading = false
    var limit = 20
    var lastPage = false
    
    func loadInitialBlogPost() {
        if (isDownloading) {
            self.refreshControl?.endRefreshing()
            return
        }
        let url = "http://popconasia.com/wp-json/wp/v2/posts?page=1&per_page=\(self.limit)"
        lastPage = false
        isDownloading = true
        tableView.reloadData()
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            self.isDownloading = false
            self.refreshControl?.endRefreshing()
            
            guard response.result.isSuccess else {
                print(response.result.error!)
                return
            }
            
            self.blogPosts.removeAll()
            
            if (response.result.value as! [Any]).count < self.limit {
                self.lastPage = true
            }
            
            for object in response.result.value as! [Any] {
                if let blogPost = BlogPost(JSON: object as! [String: Any]) {
                    self.blogPosts.append(blogPost)
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
    func loadNextBlogPost() {
        if isDownloading {
            self.refreshControl?.endRefreshing()
            return
        }
        let page = (blogPosts.count / limit) + 1
        let url = "http://popconasia.com/wp-json/wp/v2/posts?page=\(page)&per_page=\(self.limit)"
        isDownloading = true
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            self.isDownloading = false
            guard response.result.isSuccess else {
                self.tableView.reloadData()
                print(response.result.error!)
                return
            }
            
            if (response.result.value as! [Any]).count < self.limit {
                self.lastPage = true
            }
            
            for object in response.result.value as! [Any] {
                if let blogPost = BlogPost(JSON: object as! [String: Any]) {
                    self.blogPosts.append(blogPost)
                }
            }
            
            self.tableView.reloadData()
            let row = (page - 1) * self.limit
            if row < self.blogPosts.count {
                let indexPath = IndexPath(row: row, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
            }
        })
    }
    
    override func viewDidLoad() {
        loadInitialBlogPost()
        super.viewDidLoad()
        self.refreshControl?.addTarget(self, action: #selector(self.loadInitialBlogPost), for: .valueChanged)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isDownloading {
            return blogPosts.count + 1
        } else {
            return self.blogPosts.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !isDownloading && indexPath.row == blogPosts.count - 1 && !lastPage {
            loadNextBlogPost()
        }
        
        if(isDownloading && indexPath.row == blogPosts.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath)
            let spinner = cell.viewWithTag(1) as! UIActivityIndicatorView
            spinner.startAnimating()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BlogPostTableViewCell
            configure(for: cell, with: blogPosts[indexPath.row], at: indexPath)
            return cell
        }
    }
    
    func configure(for cell: BlogPostTableViewCell, with post: BlogPost, at indexPath: IndexPath) {
        if let title = try? SwiftSoup.parse(post.title!) {
            cell.postTitle.text = try? title.text()
        }
        cell.postImage.af_setImage(withURL: URL(string: post.featuredMediaUrl!)!)
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPost" {
            let controller = segue.destination as! NewsDetailTableViewController
            
            if let indexPath = tableView.indexPath(for: sender as! BlogPostTableViewCell) {
                let post = blogPosts[indexPath.row]
                var postTitle: String? = nil
                if let title = try? SwiftSoup.parse(post.title!) {
                    postTitle = try? title.text()
                }
                if post.id != nil {
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemID: "\(post.id!)" as NSObject,
                        AnalyticsParameterItemName: "\(postTitle ?? "-")" as NSObject,
                        AnalyticsParameterContentType: "BlogPost" as NSObject
                        ])
                }
                controller.blogPost = blogPosts[indexPath.row]
            }
        }
    }
    
    
}
