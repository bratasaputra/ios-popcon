//
//  ExhibitorViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/10/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup

class ExhibitorDetailViewController: UIViewController {
    var exhibitor: Exhibitor!

    @IBOutlet weak var exhibitorImage: UIImageView!
    @IBOutlet weak var exhibitorLabel: UILabel!
    @IBOutlet weak var exhibitorBoothNumber: UILabel!
    @IBOutlet weak var exhibitorDesc: UILabel!
    @IBAction func visitWebsite(_ sender: UIButton) {
        if let url = URL(string: exhibitor.link!) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBOutlet weak var websiteButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        exhibitorImage.layer.cornerRadius = 5
        exhibitorImage.layer.masksToBounds = true
        if exhibitor.featuredMediaUrl != nil {
            exhibitorImage.af_setImage(withURL: URL(string: exhibitor.featuredMediaUrl!)!)
        } else {
            exhibitorImage.image = #imageLiteral(resourceName: "popo")
        }
        exhibitorBoothNumber.text = "Booth: \(exhibitor.boothNumber ?? "") "
        if exhibitor.boothNumber2 != nil {
            exhibitorBoothNumber.text = exhibitorBoothNumber.text?.appending("- \(exhibitor.boothNumber2!)")
        }
        if let content = try? SwiftSoup.parse(exhibitor.content!) {
            exhibitorDesc.text = try? content.text()
        }
        if let title = try? SwiftSoup.parse(exhibitor.title!) {
            exhibitorLabel.text = try? title.text()
        }
        if let _ = URL(string: exhibitor.link!) {
            websiteButton.isHidden = false
        } else {
            websiteButton.isHidden = true
        }
    }
}
