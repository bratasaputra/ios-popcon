//
//  ScheduleCardCell.swift
//  popcon
//
//  Created by Brata Saputra on 7/22/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import MaterialComponents

class ScheduleCardCell: MDCCollectionViewCell {

    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var scheduleTitle: UILabel!
    @IBOutlet weak var scheduleDesc: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
        scheduleTitle.font = MDCTypography.headlineFont()
        scheduleTitle.alpha = MDCTypography.headlineFontOpacity()
        
        scheduleDesc.font = MDCTypography.body1Font()
        scheduleDesc.alpha = MDCTypography.body1FontOpacity()
        
        startTime.font = MDCTypography.body2Font()
        startTime.alpha = MDCTypography.body2FontOpacity()
        
        fromLabel.font = MDCTypography.body2Font()
        fromLabel.alpha = MDCTypography.body2FontOpacity()
        
        endTime.font = MDCTypography.body2Font()
        endTime.alpha = MDCTypography.body2FontOpacity()
        
        toLabel.font = MDCTypography.body2Font()
        toLabel.alpha = MDCTypography.body2FontOpacity()
    }

}
