//
//  Speaker.swift
//  popcon
//
//  Created by Brata Saputra on 5/11/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class Speaker: Object {
    dynamic var id: Int = 0
    dynamic var slug: String? = nil
    dynamic var link: String? = nil
    dynamic var links: String? = nil
    dynamic var title: String? = nil
    dynamic var content: String? = nil
    dynamic var featuredMediaUrl: String? = nil
    dynamic var jabatan: String? = nil
    dynamic var organisasi: String? = nil
    
    convenience init(id: Int, slug: String?, link: String?, links: String?, title: String?, content: String?, featuredMediaUrl: String?, jabatan: String?, organisasi: String?) {
        self.init()
        self.id = id
        self.slug = slug
        self.link = link
        self.links = links
        self.title = title
        self.content = content
        self.featuredMediaUrl = featuredMediaUrl
        self.jabatan = jabatan
        self.organisasi = organisasi
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func Parse(from json: [String: Any]) -> Speaker? {
        let speaker = Speaker()
        
        if let id = json["id"] as? Int {
            speaker.id = id
        } else {
            return nil
        }
        if let slug = json["slug"] as? String {
            speaker.slug = slug
        }
        if let link = json["link"] as? String {
            speaker.link = link
        }
        if let links = json["links"] as? String {
            speaker.links = links
        }
        if let title = json["title"] as? [String: Any] {
            if let rendered = title["rendered"] as? String {
                speaker.title = rendered
            }
        }
        if let content = json["content"] as? [String:Any] {
            if let rendered = content["rendered"] as? String {
                speaker.content = rendered
            }
        }
        if let jabatan = json["jabatan"] as? String {
            speaker.jabatan = jabatan
        }
        if let organisasi = json["organisasi"] as? String {
            speaker.organisasi = organisasi
        }
        if let featuredMediaUrl = json["featured_media_url"] as? String {
            speaker.featuredMediaUrl = featuredMediaUrl
        }
        return speaker
    }
}
