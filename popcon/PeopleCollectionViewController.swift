//
//  PeopleCollectionViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/11/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import Realm
import RealmSwift
import MaterialComponents
import Firebase

private let reuseIdentifier = "SpeakerCell"

class PeopleCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var speakerList: [Speaker]!
    var speakerListTemp: [Speaker]!
    var isDownloading: Bool!
    var limit: Int!
    var currentPage: Int!
    var realm: Realm!
    var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var refreshAllButton: UIBarButtonItem!
    
    @IBAction func refreshAll(_ sender: UIBarButtonItem) {
        speakerList.removeAll()
        speakerListTemp.removeAll()
        currentPage = 0
        collectionView?.reloadData()
        loadContent()
    }
    
    func loadContent() {
        if speakerList.isEmpty {
            activityIndicator?.startAnimating()
        }
        isDownloading = true
        refreshAllButton.isEnabled = false
        currentPage = currentPage + 1
        let url = "http://popconasia.com/wp-json/wp/v2/speaker/?page=\(currentPage!)&per_page=\(limit!)"
        
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            
            guard response.result.isSuccess else {
                self.reloadData()
                print(response.result.error!)
                return
            }
            
            for obj in response.result.value as! [Any] {
                if let speaker = Speaker.Parse(from: obj as! [String: Any]) {
                    try! self.realm.write {
                        self.speakerListTemp.append(speaker)
                    }
                }
            }
            
            if (response.result.value as! [Any]).count == self.limit {
                self.loadContent()
            } else {
                try! self.realm.write {
                    self.realm.delete(self.realm.objects(Speaker.self))
                    self.realm.add(self.speakerListTemp, update: true)
                }
                self.reloadData()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        speakerList = [Speaker]()
        speakerListTemp = [Speaker]()
        currentPage = 0
        limit = 100
        isDownloading = false
        realm = try! Realm()
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.collectionView?.backgroundView = self.activityIndicator
//        reloadData()
        
        if self.speakerList.isEmpty {
            loadContent()
        }
    }
    
    func reloadData() {
        self.isDownloading = false
        self.activityIndicator?.stopAnimating()
        self.refreshAllButton.isEnabled = true
        self.speakerList.removeAll()
        let list = realm.objects(Speaker.self)
        self.speakerList.append(contentsOf: list)
        self.collectionView?.reloadData()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return speakerList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
        let content = speakerList[indexPath.row]
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        imageView.image = nil
        let size = CGSize(width: imageView.frame.width, height: imageView.frame.width)
        let filter = AspectScaledToFitSizeFilter(size: size)
        let placeholder = #imageLiteral(resourceName: "popo").af_imageAspectScaled(toFit: size)
        if let url = URL(string: content.featuredMediaUrl!) {
            imageView.af_setImage(withURL: url, placeholderImage: placeholder, filter: filter);
        }
        let nameLabel = cell.viewWithTag(2) as! UILabel
        nameLabel.text = content.title
        let jabatanLabel = cell.viewWithTag(3) as! UILabel
        jabatanLabel.text = content.jabatan
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = Int.init(collectionView.frame.width)
        let totalEmptySpacing = 24
        let remainingAvailableWidth = viewWidth - totalEmptySpacing
        let newCellWidth = remainingAvailableWidth / 2
        if newCellWidth >= 145 {
            let newHeight = newCellWidth + 70
            return CGSize(width: newCellWidth, height: newHeight)
        } else {
            return CGSize(width: 145, height: 230)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPeople" {
            let controller = segue.destination as! PeopleDetailViewController
            
            if let indexPath = self.collectionView?.indexPath(for: sender as! UICollectionViewCell) {
                let people = self.speakerList[indexPath.row]
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID: "\(people.id)" as NSObject,
                    AnalyticsParameterItemName: "\(people.title ?? "-")" as NSObject,
                    AnalyticsParameterContentType: "People" as NSObject
                    ])
                controller.people = people
            }
        }
    }
}
