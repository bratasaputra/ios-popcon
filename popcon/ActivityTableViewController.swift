//
//  ActivityTableViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/9/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import Realm
import RealmSwift
import MaterialComponents
import Firebase

class ActivitiyTableViewCell : UITableViewCell {
    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityTitle: UILabel!
}

class ActivityTableViewController: UITableViewController {
    var activityList = [Activities]()
    var isDownloading = false
    var lastPage = false
    var limit = 100
    var realm: Realm!
    var activityIndicator: UIActivityIndicatorView?

    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        activityList.removeAll()
        tableView.reloadData()
        loadInitialActivity()
    }
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    func loadInitialActivity() {
        lastPage = false
        isDownloading = true
        tableView.reloadData()
        activityIndicator?.startAnimating()
        refreshButton.isEnabled = false
        let url = "http://popconasia.com/wp-json/wp/v2/activities/?page=1&per_page=\(limit)"
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            self.isDownloading = false
            self.refreshButton.isEnabled = true
            self.activityIndicator?.stopAnimating()
            guard response.result.isSuccess else {
                print(response.result.error!)
                return
            }
            
            if (response.result.value as! [Any]).count < self.limit {
                self.lastPage = true
            }
            
            self.activityList.removeAll()
            for object in response.result.value as! [Any] {
                if let activity = Activities.Parse(from: object as! [String: Any]) {
                    self.activityList.append(activity)
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.backgroundView = activityIndicator
        realm = try! Realm()
        loadInitialActivity()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return activityList.count == 0 ? 0 : 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ActivitiyTableViewCell
        configure(for: cell, with: activityList[indexPath.row], at: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func configure(for cell: ActivitiyTableViewCell, with activity: Activities, at indexPath: IndexPath) {
        if let title = try? SwiftSoup.parse(activity.title!) {
            cell.activityTitle.text = try? title.text()
        }
        cell.activityImage.af_cancelImageRequest()
        cell.activityImage.image = nil
        cell.activityImage.af_setImage(withURL: URL(string: activity.featuredMediaUrl!)!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowEvent" {
            let controller = segue.destination as! ActivityDetailViewController
            
            if let indexPath = tableView.indexPath(for: sender as! ActivitiyTableViewCell) {
                let activity = activityList[indexPath.row]
                var activityTitle: String? = nil
                if let title = try? SwiftSoup.parse(activity.title!) {
                    activityTitle = try? title.text()
                }
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID: "\(activity.id)" as NSObject,
                    AnalyticsParameterItemName: "\(activityTitle ?? "-")" as NSObject,
                    AnalyticsParameterContentType: "Activity" as NSObject
                    ])
                controller.activity = activityList[indexPath.row]
            }
        }
    }
}
