//
//  ScheduleTableViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/9/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup

class Item {
    var id: String?
    var title: String?
    var desc: String?
    var start: String?
    var end: String?
    var location: Int?
}

class ScheduleTableViewController: UITableViewController {
    var scheduleList: [Schedule]!
    var isDownloading = false
    var lastPage = false
    var limit = 10
    
    var dateList: [String]!
    var itemList: [[Item]]!
    var location: String!
    var titleString: String!
    var activityIndicator: UIActivityIndicatorView?
    
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        loadSchedule()
    }
    
    func loadSchedule() {
        activityIndicator?.startAnimating()
        refreshButton.isEnabled = false
        self.dateList.removeAll()
        self.itemList.removeAll()
        self.tableView.reloadData()
        Alamofire.request("http://popconasia.com/wp-json/wp/v2/schedule_composed").responseJSON(completionHandler: {
            response in
            self.activityIndicator?.stopAnimating()
            self.refreshButton.isEnabled = true
            guard response.result.isSuccess else {
                print(response.result.error!)
                return
            }
            
            if let object = response.result.value as? [String: Any] {
                if let location = object[self.location] as? [String: Any] {
                    if let fourAug = location["2017-08-04"] as? [Any] {
                        self.dateList.append("04 August 2017")
                        var list = [Item]()
                        for item in fourAug {
                            list.append(self.parse(from: item as! [String: Any]))
                        }
                        self.itemList.append(list)
                    }
                    
                    if let fifthAug = location["2017-08-05"] as? [Any] {
                        self.dateList.append("05 August 2017")
                        var list = [Item]()
                        for item in fifthAug {
                            list.append(self.parse(from: item as! [String: Any]))
                        }
                        self.itemList.append(list)
                    }
                    
                    if let sixAug = location["2017-08-06"] as? [Any] {
                        self.dateList.append("06 August 2017")
                        var list = [Item]()
                        for item in sixAug {
                            list.append(self.parse(from: item as! [String: Any]))
                        }
                        self.itemList.append(list)
                    }
                }
            }
            self.tableView.reloadData()
        })
    }
    
    private func parse(from json: [String: Any]) -> Item {
        let item = Item()
        if let timeStart = json["time_start"] as? String {
            item.start = timeStart
        }
        if let timeEnd = json["time_end"] as? String {
            item.end = timeEnd
        }
        if let object = json["post_object"] as? [String: Any] {
            if let title = object["post_title"] as? String {
                item.title = title
            }
            if let content = object["post_content"] as? String {
                item.desc = content
            }
            if let id = object["ID"] as? String {
                item.id = id
            }
        }
        return item
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateList = [String]()
        itemList = [[Item]]()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.backgroundView = activityIndicator
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.title = titleString
        
        loadSchedule()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dateList.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let item = itemList[indexPath.section][indexPath.row]
        
        let timeLabel = cell.viewWithTag(1) as! UILabel
        timeLabel.text = "\(item.start!) - \(item.end!)"
        
        let titleLabel = cell.viewWithTag(2) as! UILabel
        titleLabel.text = item.title
        
        let descLabel = cell.viewWithTag(3) as! UILabel
        descLabel.text = item.desc
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dateList[section]
    }
}
