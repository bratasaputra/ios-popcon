//
//  ActivityCardCell.swift
//  popcon
//
//  Created by Brata Saputra on 7/24/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import MaterialComponents

class ActivityCardCell: UICollectionViewCell {

    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityTitle: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
        activityTitle.font = MDCTypography.titleFont()
        activityTitle.alpha = MDCTypography.titleFontOpacity()
    }

}
