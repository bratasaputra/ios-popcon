//
//  SpeakerDetailViewController.swift
//  popcon
//
//  Created by Brata Saputra on 5/11/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup
import SafariServices

class PeopleDetailViewController: UIViewController, UIWebViewDelegate {
    var people: Speaker!

    @IBOutlet weak var peopleImage: UIImageView!
    @IBOutlet weak var peopleInfo: UILabel!
    @IBOutlet weak var peopleDescWebView: UIWebView!
    @IBOutlet weak var peopleDescWebViewConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        title = people.title
        peopleDescWebView.loadHTMLString(people.content!, baseURL: nil);
        peopleDescWebView.scrollView.isScrollEnabled = false
        peopleDescWebView.delegate = self
        peopleInfo.text = "\(people.jabatan ?? "-"), \(people.organisasi ?? "-")"
        if let url = URL(string: people.featuredMediaUrl!) {
            peopleImage.af_cancelImageRequest()
            peopleImage.layer.removeAllAnimations()
            peopleImage.layer.cornerRadius = 5
            peopleImage.layer.masksToBounds = true
            peopleImage.image = nil
            peopleImage.af_setImage(withURL: url)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        peopleDescWebViewConstraint.constant = webView.scrollView.contentSize.height
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request)
        if navigationType == .linkClicked {
            let svc = SFSafariViewController(url: request.url!)
            self.present(svc, animated: true, completion: nil)
            return false
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
