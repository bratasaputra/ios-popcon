//
//  PromotionDetailViewController.swift
//  popcon
//
//  Created by Brata Saputra on 7/27/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSoup

class PromotionDetailViewController: UIViewController, UIWebViewDelegate {
    var promotion: Promotion!
    
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var promoImageHeight: NSLayoutConstraint!
    @IBOutlet weak var promoDetail: UIWebView!
    @IBOutlet weak var promoDetailHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let promoTitle = try? SwiftSoup.parse(promotion.title!) {
            self.title = try? promoTitle.text()
        }
        
        promoImage.af_cancelImageRequest()
        promoImage.layer.removeAllAnimations()
        promoImage.image = nil
        promoImage.af_setImage(withURL: URL(string: promotion.featuredMediaUrl!)!, completion: {
            response in
            
            let imageWidth = response.result.value?.size.width
            let imageHeight = response.result.value?.size.height
            let imageViewWidth = self.promoImage.frame.width
            let imageViewHeight = (imageHeight! / imageWidth!) * imageViewWidth
            self.promoImageHeight.constant = imageViewHeight
        })
        
        promoDetail.scrollView.isScrollEnabled = false
        promoDetail.delegate = self
        
        promoDetail.loadHTMLString(promotion.content!, baseURL: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        promoDetailHeight.constant = webView.scrollView.contentSize.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
