//
//  ActivityDetailViewController.swift
//  popcon
//
//  Created by Brata Saputra on 7/21/17.
//  Copyright © 2017 Brata Saputra. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SafariServices
import SwiftSoup

class ActivityDetailViewController: UIViewController, UIWebViewDelegate {
    var activity: Activities!
    
    
    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityDescConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityDesc: UIWebView!
    var downloader: ImageDownloader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let activityTitle = try? SwiftSoup.parse(activity.title!) {
            title = try? activityTitle.text()
        }
        
        activityDesc.scrollView.isScrollEnabled = false
        activityDesc.delegate = self
        activityDesc.loadHTMLString(activity.content!, baseURL: nil)
        
        downloader = ImageDownloader()
        let urlRequest = URLRequest(url: URL(string: activity.featuredMediaUrl!)!)
        
        downloader.download(urlRequest, completion: {
            response in
            
            if let image = response.result.value {
                let width = self.activityImage.frame.size.width
                let imageWidth = image.size.width
                let imageHeight = image.size.height
                let height = imageHeight/imageWidth * width
                self.activityImageConstraint.constant = height
                self.activityImage.image = image.af_imageScaled(to: CGSize(width: width, height: height))
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityDescConstraint.constant = webView.scrollView.contentSize.height
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == .linkClicked {
            let svc = SFSafariViewController(url: request.url!)
            self.present(svc, animated: true, completion: nil)
            return false
        }
        return true;
    }
}
